<script type="text/javascript">
    function popup_centralizado(pageURL, title,w,h) {
        var left = (screen.width/2)-(w/2);
        var top = (screen.height/2)-(h/2);
        var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
    }
</script>
<?php
?>
<div class="col-md-12 py-3 p-0 text-right">
    <a class="btn btn-slide site-button radius-xl fb-btn" onclick="popup_centralizado('https://www.facebook.com/sharer/sharer.php?u=<?=$url.$urlPagina?>','Compartilhar no Facebook',700,430)">
        <i class="fab fa-facebook-f"></i>
        <span>Facebook</span>
    </a>
    <a class="btn btn-slide site-button radius-xl tw-btn" onclick="popup_centralizado('https://twitter.com/intent/tweet?url=<?=$url.$urlPagina?>&amp;text=<?=$NomeEmpresa.' - '.$title;?>','Compartilhar no Twitter',700,430)" data-width="100">
        <i class="fab fa-twitter"></i>
        <span>Twitter</span>
    </a>
</div>