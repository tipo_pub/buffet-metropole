<?php
$title = 'Espaço Paris';
$description = '';
$keywords = '';
?>
<?php require_once 'includes/head.php'; ?>
<?php require_once 'includes/header.php'; ?>
<section id="espaco-sp">
	<div class="container">
		<div class="row">
			<div class="col-12 p-lg-5 text-center pt-3">
				<div class="content-espaco">
					<h1 class="text-primary text-12 font-GreatVibes">Espaço Paris</h1>
					<p>
						O <strong>Espaço Paris</strong> é um espaço para festas e eventos aconchegante e de muito bom gosto para realização de festas de casamento, 15 anos e comemorações corporativas.
					</p>
					<p>
						O <strong>Espaço Paris</strong> é mais um espaço para festas e eventos do Buffet Metropole. Possui um salão com capacidade para até 400 convidados, 350 m2 de área útil e um charmoso <strong>terraço</strong> jardim, salão receptivo e elevador próprio para acesso ao salão principal.
					</p>
					<p>
						Com capacidade para até 350 pessoas, o Espaço New York é perfeito para todo tipo de evento.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12"> 
				<h2 class="text-primary text-12 font-GreatVibes text-lg-center">Características</h2>
			</div>

			<div class="col-lg-6 pb-4">
				<ul>
					<li>Capacidade para até 400 convidados;</li>
					<li>Salão principal retangular com 350 m² de área;</li>
					<li>Charmoso <strong>terraço</strong> jardim;</li>
					<li><strong>Salão</strong> receptivo com 50 m²;</li>
					<li>Elevador com capacidade para até 9 pessoas;</li>
					<li>Ar condicionado em todos os ambientes;</li>
					<li>Cozinha montade e equipada;</li>
				</ul>
			</div>
			<div class="col-lg-6 pb-4">
				<ul>
					<li>Infraestrutura montada para gerador, sonorização, iluminação e projeção;</li>
					<li>Equipe de manutenção e limpeza;</li>
					<li>Total Acessibilidade;</li>
					<li>Estacionamento no local;</li>
					<li>Entrada e saída de serviço para montagens e desmontagem.</li>
				</ul>
			</div>
		</div>
	</div>
</section>



<section id="galeria-espaco" class="section-dark">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 p-4">
				<h4 class="font-weight-bold text-primary text-12 font-GreatVibes text-center pt-4">Galeria</h4>
			</div>
			<article class="post post-large blog-single-post border-0 m-0 p-0">

				<div class="post-image ml-0">
					<div class="lightbox text-center" data-plugin-options="{'delegate': 'a', 'type': 'image', 'gallery': {'enabled': true}, 'mainClass': 'mfp-with-zoom', 'zoom': {'enabled': true, 'duration': 300}}">
						<div class="row mx-0">

							<!-- Loop images gallery -->
							<?php 	
							$i = 1;
							$dir = count(glob("img/galeria/espacos/paris/*.jpg"));
								// echo $dir;
							while ($i <= $dir):
								?>
								<div class="col-6 col-lg-3 p-2">
									<a href="<?=$caminhoEspacos . 'paris/' . $i . '.jpg'; ?>">
										<span class="thumb-info thumb-info-no-borders thumb-info-centered-icons">
											<span class="thumb-info-wrapper">
												<img src="<?=$caminhoEspacos . 'paris/thumbs/' . $i . '.jpg';?>" class="img-fluid" loading="lazy"/>
												<span class="thumb-info-action">
													<span class="thumb-info-action-icon thumb-info-action-icon-light"><i class="fas fa-plus text-dark"></i></span>
												</span>
											</span>
										</span>
									</a>
								</div>
								<?php
								$i++; 
							endwhile;
							?>
						</div>
					</div>
				</div>
			</article>
		</div>
	</div>
</div>
</div>
</section>


<section id="espacos" class="section-dark border-0 pb-3">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center pt-5 pb-lg-5">
				<h3 class="font-weight-bold text-primary text-12">Outros Espaços</h3>
			</div>
			<div class="col-12 col-lg-6 align-self-center" >
				<h4 class="font-weight-normal text-10 text-brown font-GreatVibes">
					Espaço São Paulo
				</h4>
				<ul>
					<li>Capacidade para até 350 convidados;</li>
					<li>Ar condicionado central;</li>
					<li>Cozinha exclusiva;</li>
					<li>Controle de som e iluminação por computador;</li>
					<li>Serviço de Vallet;</li>
					<li>Sala exclusiva para Noiva ou Debutante.</li>
				</ul>
				<a href="<?=$url ?>espaco-sao-paulo" class="link-espaco">Para mais informações clique aqui</a>
					<!-- </div>	
						<div class="col-12 col-lg-8 pb-lg-3"> -->
							<div class="slider-espaco pt-3">
								<?php  
								$i = 1;
						// while($i <= 1):
								$imgEspaco =  $pastaEspaco. 'sao-paulo/'. $i . '.jpg';

								echo '<img data-lazy="'.$imgEspaco.'" alt="Espaço Paris" loading="lazy">';

						// 	$i++;
						// endwhile;
								?>

							</div>
						</div>

						<div class="col-12 col-lg-6">
							<h4 class="font-weight-normal text-10 text-brown font-GreatVibes">
								Espaço New York
							</h4>
							<ul>
								<li>Capacidade para até 350 convidados;</li>
								<li>Ar condicionado central;</li>
								<li>Cozinha exclusiva;</li>
								<li>Controle de som e iluminação por computador;</li>
								<li>Serviço de Vallet;</li>
								<li>Sala exclusiva para Noiva ou Debutante.
								</li>
							</ul>
							<a href="<?=$url ?>espaco-new-york" class="link-espaco">Para mais informações clique aqui</a>
						<!-- </div>	
							<div class="col-12 col-lg-8 pb-lg-3"> -->
								<div class="slider-espaco pt-3">
									<?php  
									$i = 1;
									while($i <= 3):
										$imgEspaco =  $pastaEspaco. 'new-york/'. $i . '.jpg';
										echo '<img data-lazy="'.$imgEspaco.'" alt="Espaço São Paulo" loading="lazy">';
										$i++;
									endwhile;
									?>
								</div>
							</div>
						</div>
					</div>
				</section>



				<section id="localizacao">
					<div class="col-12 p-4">
						<h4 class="font-weight-bold text-primary text-12 font-GreatVibes text-center pt-4">Localização</h4>
					</div>
					<div class="mapouter">
						<div class="gmap_canvas">
							<iframe width="100%" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=Av.%20Engenheiro%20Caetano%20%C3%81lvares%2C%20510%20-%20Lim%C3%A3o%2C%20S%C3%A3o%20Paulo%2C%20SP.&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
						</div>
					</div>
				</section>

				<?php 
				require 'includes/footer.php';
				?>