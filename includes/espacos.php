<section id="spaces">
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-6 p-0">
			<section class="parallax section section-parallax custom-parallax-bg-pos-left custom-sec-left h-100 m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'horizontalPosition': '100%'}" data-image-src="<?=$pastaImg?>espaco-sao-paulo.jpg" style="min-height: 315px;">
			</section>
		</div>
		<div class="col-lg-6 p-0">
			<section class="section section-light section-no-border h-100 m-0">
				<div class="row m-0">
					<div class="col-half-section col-half-section-left">
						<h2 class="font-weight-bold text-11 font-GreatVibes">Espaço São Paulo</h2>
						
						<ul class="list list-icons list-primary">
							<li><i class="fas fa-check"></i> Capacidade para até 250 convidados;</li>
							<li><i class="fas fa-check"></i> Ar condicionado Central;</li>
							<li><i class="fas fa-check"></i> Cozinha exclusiva;</li>
							<li><i class="fas fa-check"></i> Serviço de Vallet;</li>
							<li><i class="fas fa-check"></i> Sala exclusiva para Noiva ou Debutante;</li>
							<li><i class="fas fa-check"></i> Som e iluminação controlados por computador.</li>
						</ul>

						<a href="<?=$url?>espaco-sao-paulo" class="btn btn-outline btn-primary custom-btn-style-2 font-weight-semibold text-color-dark text-uppercase mt-2" title="Mais Informações sobre o Espaço São Paulo"><i class="fa fa-plus"></i> Informações</a>
					</div>
				</div>
			</section>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 order-2 order-lg-1 p-0">
			<section class="section section-light section-no-border h-100 m-0">
				<div class="row justify-content-end m-0">
					<div class="col-half-section col-half-section-right custom-text-align-right">
						<h2 class="font-weight-bold text-11 font-GreatVibes">Espaço New York</h2>
						
						<ul class="list list-icons list-primary">
							<li><i class="fas fa-check"></i> Capacidade para até 350 convidados;</li>
							<li><i class="fas fa-check"></i> Ar condicionado Central;</li>
							<li><i class="fas fa-check"></i> Cozinha exclusiva;</li>
							<li><i class="fas fa-check"></i> Serviço de Vallet;</li>
							<li><i class="fas fa-check"></i> Sala exclusiva para Noiva ou Debutante;</li>
							<li><i class="fas fa-check"></i> Som e iluminação controlados por computador.</li>
						</ul>

						<a href="<?=$url?>espaco-new-york" class="btn btn-outline btn-primary custom-btn-style-2 font-weight-semibold text-color-dark text-uppercase mt-2" title="Mais Informações sobre o Espaço New York"><i class="fa fa-plus"></i> Informações</a>
					</div>
				</div>
			</section>
		</div>
		<div class="col-lg-6 order-1 order-lg-2 p-0">
			<section class="parallax section section-parallax h-100 m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'horizontalPosition': '100%'}" data-image-src="<?=$pastaImg?>espaco-new-york.jpg" style="min-height: 315px;">
			</section>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 p-0">
			<section class="parallax section section-parallax custom-parallax-bg-pos-left custom-sec-left h-100 m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5, 'horizontalPosition': '100%'}" data-image-src="<?=$pastaImg?>espaco-paris.jpg" style="min-height: 315px;">
			</section>
		</div>
		<div class="col-lg-6 p-0">
			<section class="section section-light section-no-border h-100 m-0">
				<div class="row m-0">
					<div class="col-half-section col-half-section-left">
						<h2 class="font-weight-bold text-11 font-GreatVibes">Espaço Paris</h2>
						
						<ul class="list list-icons list-primary">
							<li><i class="fas fa-check"></i> Capacidade para até 400 convidados;</li>
							<li><i class="fas fa-check"></i> Ar condicionado Central;</li>
							<li><i class="fas fa-check"></i> Salão receptivo com 50 m²;</li>
							<li><i class="fas fa-check"></i> Salão principal retangular com 350 m² de área;</li>
							<li><i class="fas fa-check"></i> Sala exclusiva para Noiva ou Debutante;</li>
							<li><i class="fas fa-check"></i> Charmoso terraço jardim;</li>
							<li><i class="fas fa-check"></i> Elevador com capacidade para até 9 pessoas.</li>
						</ul>

						<a href="<?=$url?>espaco-paris" class="btn btn-outline btn-primary custom-btn-style-2 font-weight-semibold text-color-dark text-uppercase mt-2" title="Mais Informações sobre o Espaço Paris"><i class="fa fa-plus"></i> Informações</a>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
</section>
