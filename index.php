<?php
$title = 'Home';
$description = '';
$keywords = '';
?>
<?php require_once 'includes/head.php'; ?>
<?php require_once 'includes/header.php'; ?>
<section class="section py-5 my-0">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="heading heading-border heading-middle-border heading-middle-border-center">
					<h2 class="font-weight-bold text-primary text-12 font-GreatVibes">Sobre o Metropole</h2>
				</div>


				<div class="row">
					<div class="col-lg-6 pb-sm-4 pb-lg-0 pr-lg-5 mb-sm-5 mb-lg-0 mt-lg-5">
						<p>O Metropole é um buffet com estrutura completa para eventos.</p>

						<p>Somos especializados em Festas Debutantes, bem como, Casamentos, Bodas, Corporativos e Formaturas.</p>

						<p>Possuímos localização privilegiada na cidade de São Paulo para fazer seu sonho acontecer.</p>

						<p>Sua satisfação é nosso maior patrimônio, o bem mais valioso que uma empresa do ramo pode ter!</p>
					</div>
					<div class="col-sm-8 col-md-6 col-lg-4 offset-sm-4 offset-md-4 offset-lg-2 mt-sm-5" style="top: 1.7rem;">
						<img src="<?=$pastaImg?>espaco-sao-paulo.jpg" class="img-fluid position-absolute d-none d-sm-block appear-animation" data-appear-animation="expandIn" data-appear-animation-delay="300" style="top: 10%; left: -50%;" alt="Buffet São Paulo" />
						<img src="<?=$pastaImg?>espaco-new-york.jpg" class="img-fluid position-absolute d-none d-sm-block appear-animation" data-appear-animation="expandIn" style="top: -33%; left: -29%;" alt="Buffet New York" />
						<img src="<?=$pastaImg?>espaco-paris.jpg" class="img-fluid position-relative appear-animation mb-2" data-appear-animation="expandIn" data-appear-animation-delay="600" alt="Buffet Paris" />
					</div>
				</div>

				<div class="featured-boxes featured-boxes-style-4 py-5 mt-5">
					<div class="row">
						<div class="col-lg-3">
							<div class="featured-box featured-box-primary appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="300">
								<div class="box-content p-0">
									<i class="icon-featured text-13 fab fa-fort-awesome"></i>
									<h4 class="font-weight-normal text-7 font-GreatVibes">Estrutura <strong class="font-weight-extra-bold">Completa</strong></h4>
									<p class="mb-0">Possuímos <strong>3 espaços</strong> para festas com capacidade para até <strong>400 pessoas</strong>, <strong>infraestrutura completa</strong> e <strong>serviços de alta qualidade</strong>.</p>
								</div>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="featured-box featured-box-primary appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="600">
								<div class="box-content p-0">
									<i class="icon-featured text-13 fas fa-map-marker-alt"></i>
									<h4 class="font-weight-normal text-7 font-GreatVibes">Localização <strong class="font-weight-extra-bold">Privilegiada</strong></h4>
									<p class="mb-0">Estamos próximos à ponte do Limão, à <strong>50 metros da Marginal Tietê</strong>, com fácil acesso as principais vias que ligam todas as regiões de São Paulo.</p>
								</div>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="featured-box featured-box-primary text-center appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="900">
								<div class="box-content p-0">
									<i class="icon-featured text-13 fas fa-user-tie"></i>
									<h4 class="font-weight-normal text-7 font-GreatVibes">Assessoria <strong class="font-weight-extra-bold">Profissional</strong></h4>
									<p class="mb-0">Sua festa é realizada por <strong>profissionais experientes</strong> de <strong>alta qualidade técnica</strong> e acompanhada em <strong>detalhes</strong> até os últimos minutos do evento.</p>
								</div>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="featured-box featured-box-primary appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="1200">
								<div class="box-content p-0">
									<i class="icon-featured text-13 fas fa-utensils"></i>
									<h4 class="font-weight-normal text-7 font-GreatVibes"><strong class="font-weight-extra-bold">Gastronomia</strong></h4>
									<p class="mb-0">Contamos com <strong>renomado serviço gastronômico</strong> para engrandecer o evento e <strong>satisfazer os paladares mais exigentes</strong>.</p>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include 'includes/espacos.php'; ?>

<section class="section section-dark border-0 p-5 m-0">
	<div class="container">
		<div class="heading heading-border heading-middle-border heading-middle-border-center">
			<h2 class="font-weight-bold text-primary text-12 font-GreatVibes">Galeria</h2>
		</div>
		<?php $qntPag = "contIndex"; include 'includes/galeria.php' ;?>
	</div>
</section>

<?php require_once 'includes/footer.php'; ?>