<?php
$title = 'Depoimentos';
$description = '';
$keywords = '';
?>
<?php require_once 'includes/head.php'; ?>
<?php require_once 'includes/header.php'; ?>
<section id="depoimentos">
<!-- 	<div class="container-fluid pb-2">
		<div class="row mb-3 pt-5">
			<div class="col-lg-4">
				<div class="testimonial testimonial-style-5">
					<blockquote>
						<p class="mb-0">"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget risus porta, tincidunt turpis at, interdum tortor. Suspendisse potenti. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sociis natoque penatibus et magnis dis parturient montes"</p>
					</blockquote>
					<div class="testimonial-arrow-down"></div>
					<div class="testimonial-author">
						<p><strong class="font-weight-extra-bold pt-2">John Smith</strong><span>CEO & Founder - Okler</span></p>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="testimonial testimonial-style-5">
					<blockquote>
						<p class="mb-0">"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget risus porta, tincidunt turpis at, interdum tortor. Suspendisse potenti. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sociis natoque penatibus et magnis dis parturient montes"</p>
					</blockquote>
					<div class="testimonial-arrow-down"></div>
					<div class="testimonial-author">
						<p><strong class="font-weight-extra-bold pt-2">John Smith</strong><span>CEO & Founder - Okler</span></p>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="testimonial testimonial-style-5">
					<blockquote>
						<p class="mb-0">"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget risus porta, tincidunt turpis at, interdum tortor. Suspendisse potenti. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sociis natoque penatibus et magnis dis parturient montes"</p>
					</blockquote>
					<div class="testimonial-arrow-down"></div>
					<div class="testimonial-author">
						<p><strong class="font-weight-extra-bold pt-2">John Smith</strong><span>CEO & Founder - Okler</span></p>
					</div>
				</div>
			</div>
		</div>
	</div> -->
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-10">
				<div class="owl-carousel owl-theme nav-bottom rounded-nav" data-plugin-options="{'items': 1, 'loop': false}">
					<div>
						<div class="testimonial testimonial-style-2 testimonial-with-quotes mb-0">
							<div class="testimonial-author">
								<img src="img/clients/client-1.jpg" class="img-fluid rounded-circle" alt="">
							</div>
							<blockquote>
								<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget risus porta, tincidunt turpis at, interdum tortor. Suspendisse potenti. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce ante tellus, convallis non consectetur sed, pharetra nec ex.</p>
							</blockquote>
							<div class="testimonial-author">
								<p><strong class="font-weight-extra-bold">John Smith</strong><span>CEO & Founder - Okler</span></p>
							</div>
						</div>
					</div>
					<div>
						<div class="testimonial testimonial-style-2 testimonial-with-quotes mb-0">
							<div class="testimonial-author">
								<img src="img/clients/client-1.jpg" class="img-fluid rounded-circle" alt="">
							</div>
							<blockquote>
								<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget risus porta, tincidunt turpis at, interdum tortor. Suspendisse potenti. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							</blockquote>
							<div class="testimonial-author">
								<p><strong class="font-weight-extra-bold">John Smith</strong><span>CEO & Founder - Okler</span></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="testimonial testimonial-primary">
					<blockquote>
						<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat.</p>
					</blockquote>
					<div class="testimonial-arrow-down"></div>
					<div class="testimonial-author">
						<div class="testimonial-author-thumbnail">
							<img src="img/clients/client-1.jpg" class="rounded-circle" alt="">
						</div>
						<p><strong class="font-weight-extra-bold">John Smith</strong><span>CEO & Founder - Okler</span></p>
					</div>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="testimonial testimonial-primary">
					<blockquote>
						<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat.</p>
					</blockquote>
					<div class="testimonial-arrow-down"></div>
					<div class="testimonial-author">
						<div class="testimonial-author-thumbnail">
							<img src="img/clients/client-1.jpg" class="rounded-circle" alt="">
						</div>
						<p><strong class="font-weight-extra-bold">John Smith</strong><span>CEO & Founder - Okler</span></p>
					</div>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="testimonial testimonial-primary">
					<blockquote>
						<p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat.</p>
					</blockquote>
					<div class="testimonial-arrow-down"></div>
					<div class="testimonial-author">
						<div class="testimonial-author-thumbnail">
							<img src="img/clients/client-1.jpg" class="rounded-circle" alt="">
						</div>
						<p><strong class="font-weight-extra-bold">John Smith</strong><span>CEO & Founder - Okler</span></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="espacos" class="section-dark border-0 pb-3">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 text-center pt-5 pb-lg-4">
				<h3 class="font-weight-bold text-primary text-12">Nossos Espaços</h3>
				<p>O Buffet Metropole possui 3 espaços distintos para agradar aos mais variados estilos.</p>
			</div>
			<div class="col-12 col-lg-4">
				<h4 class="font-weight-normal text-10 text-brown font-GreatVibes">
					Espaço New York
				</h4>
					<ul>
						<li>Capacidade para até 350 convidados</li>
						<li>Ar condicionado central</li>
						<li>Cozinha exclusiva</li>
						<li>Controle de som e iluminação por computador</li>
						<li>Serviço de Vallet</li>
						<li>Sala exclusiva para Noiva ou Debutante
						</li>
					</ul>
					<a href="<?=$url ?>espaco-new-york" class="link-espaco">Para mais informações clique aqui</a>
					<!-- </div>	
						<div class="col-12 col-lg-8 pb-lg-3"> -->
							<div class="slider-espaco pt-3">
								<?php  
								$i = 1;
								while($i <= 3):
									$imgEspaco =  $pastaEspaco. 'new-york/'. $i . '.jpg';
									echo '<img data-lazy="'.$imgEspaco.'" alt="Espaço New York" loading="lazy">';
									$i++;
								endwhile;
								?>
							</div>
						</div>
						<div class="col-12 col-lg-4">
							<h4 class="font-weight-normal text-10 text-brown font-GreatVibes">
								Espaço Paris
							</h4>
							<ul>
								<li>Capacidade para até 400 convidados;</li>
								<li>Salão principal retangular com 350 m² de área;</li>
								<li>Charmoso terraço jardim;</li>
								<li>Salão receptivo com 50 m²;</li>
								<li>Elevador com capacidade para até 9 pessoas;</li>
								<li>Ar condicionado central e sala exclusiva para Noiva ou Debutante.
								</li>

							</ul>
							<a href="<?=$url ?>espaco-paris" class="link-espaco">Para mais informações clique aqui</a>
					<!-- </div>	
						<div class="col-12 col-lg-8 pb-lg-3"> -->
							<div class="slider-espaco pt-3">
								<?php  
								$i = 1;
						// while($i <= 1):
								$imgEspaco =  $pastaEspaco. 'paris/'. $i . '.jpg';

								echo '<img data-lazy="'.$imgEspaco.'" alt="Espaço New York" loading="lazy">';

						// 	$i++;
						// endwhile;
								?>

							</div>
						</div>

						<div class="col-12 col-lg-4">
							<h4 class="font-weight-normal text-10 text-brown font-GreatVibes">
								Espaço São Paulo
							</h4>
								<ul>
									<li>Capacidade para até 350 convidados;</li>
									<li>Ar condicionado central;</li>
									<li>Cozinha exclusiva;</li>
									<li>Controle de som e iluminação por computador;</li>
									<li>Serviço de Vallet;</li>
									<li>Sala exclusiva para Noiva ou Debutante.</li>
								</ul>
								<a href="<?=$url ?>espaco-sao-paulo" class="link-espaco">Para mais informações clique aqui</a>
						<!-- </div>	
							<div class="col-12 col-lg-8 pb-lg-3"> -->
								<div class="slider-espaco pt-3">
									<?php  
									$i = 1;
									while($i <= 3):
										$imgEspaco =  $pastaEspaco. 'sao-paulo/'. $i . '.jpg';
										echo '<img data-lazy="'.$imgEspaco.'" alt="Espaço São Paulo" loading="lazy">';
										$i++;
									endwhile;
									?>
								</div>
							</div>
						</div>
					</div>
				</section>

				<?php 
				require 'includes/section-galeria.php';
				require 'includes/footer.php';
				?>