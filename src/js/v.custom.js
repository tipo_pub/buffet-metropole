/* CLASSE ATIVO - MENU TOPO */
$('ul#mainNav li a[href="'+location.href+'"]').addClass('active');
if($('ul#mainNav ul li a[href="'+location.href+'"]').length) {
	$('ul#mainNav ul li a[href="'+location.href+'"]').parents('ul').parents('li').find('>a').addClass('active');
}

/* CLASSE ATIVO - MENU RODAPÉ & SIDEBAR PRODUTOS */
$('ul.lista-rodape li a[href="'+location.href+'"]').addClass('active');
if($('ul.lista-rodape ul li a[href="'+location.href+'"]').length) {
	$('ul.lista-rodape ul li a[href="'+location.href+'"]').parents('ul').parents('li').find('>a').addClass('active');
}

/* Páginação - Galeria / Produtos / Projetos */
$(document).ready(function(){

	if ($('#content').hasClass('contIndex')) {
		var show_per_page = 6;
	}
	if ($('#content').hasClass('contGaleria')) {
		var show_per_page = 12;
	}
	if ($('#content').hasClass('contProdutos')){
		var show_per_page = 24;
	}
	if ($('#content').hasClass('contProjetos')){
		var show_per_page = 5;
	}
	
	if ($('#content').hasClass('contIndex') || $('#content').hasClass('contGaleria') || $('#content').hasClass('contProdutos')){
		var number_of_items = $('#content').find("div").length;
	} else if ($('#content').hasClass('contProjetos')) {
		var number_of_items = $('#content').find("article").length;
	}
	
	var number_of_pages = Math.ceil(number_of_items/show_per_page);

	console.log("Deve mostrar: ");
	console.log(show_per_page);

	console.log("Qnd de Itens: ");
	console.log(number_of_items);

	console.log("Qnd de Itens por Página: ");
	console.log(number_of_pages);

	$('#current_page').val(0);
	$('#show_per_page').val(show_per_page);

	var navigation_html = '<ul class="pagination bootpag">';

	var current_link = 0;
	while(number_of_pages > current_link){
		navigation_html += '<li class="page-item"><a class="page-link" href="javascript:go_to_page(' + current_link +')" longdesc="' + current_link +'">'+ (current_link + 1) +'</a></li>';
		current_link++;
	}

	navigation_html += '</ul>';

	$('#portfolioPagination').html(navigation_html);

	$('#portfolioPagination .page-item:first').addClass('active');

	if ($('#content').hasClass('contIndex') || $('#content').hasClass('contGaleria') || $('#content').hasClass('contProdutos')){
		$('#content').find("div").css('display', 'none');
		$('#content').find("div").slice(0, show_per_page).css('display', 'block');
	} else if ($('#content').hasClass('contProjetos')) {
		$('#content').find("article").css('display', 'none');
		$('#content').find("article").slice(0, show_per_page).css('display', 'block');
	}	
});

function go_to_page(page_num){
	var show_per_page = parseInt($('#show_per_page').val());

	start_from = page_num * show_per_page;

	end_on = start_from + show_per_page;

	if ($('#content').hasClass('contIndex') || $('#content').hasClass('contGaleria') || $('#content').hasClass('contProdutos')){
		$('#content').find("div").css('display', 'none').slice(start_from, end_on).css('display', 'block');
	} else if ($('#content').hasClass('contProjetos')) {
		$('#content').find("article").css('display', 'none').slice(start_from, end_on).css('display', 'block');
	
	}

	$('.page-link[longdesc=' + page_num +']').parent('.page-item').addClass('active').siblings('.active').removeClass('active');

	$('#current_page').val(page_num);
}