<?php /* DISTRIBUIDOR - PARCEIROS - CLIENTES */?>
<section class="section my-0 appear-animation" style="margin: 100px 0;" data-appear-animation="fadeInDownShorter">
    <div class="container">
        <div class="row">
            <div class="col">
                <h3 class="text-center mb-3">Parceiros / Clientes</h3>
                <div class="owl-carousel owl-theme show-nav-title show-nav-title-both-sides" data-plugin-options="{'items': 5, 'margin': 20, 'loop': true, 'nav': true, 'dots': false}">
                    <div>
                        <img src="<?=$caminhoClientes?>cliente-01.png" class="img-fluid rounded" alt="Cliente - 01" />
                    </div>
                    <div>
                        <img src="<?=$caminhoClientes?>cliente-02.png" class="img-fluid rounded" alt="Cliente - 02" />
                    </div>
                    <div>
                        <img src="<?=$caminhoClientes?>cliente-03.png" class="img-fluid rounded" alt="Cliente - 03" />
                    </div>
                    <div>
                        <img src="<?=$caminhoClientes?>cliente-04.png" class="img-fluid rounded" alt="Cliente - 04" />
                    </div>
                    <div>
                        <img src="<?=$caminhoClientes?>cliente-01.png" class="img-fluid rounded" alt="Cliente - 01" />
                    </div>
                    <div>
                        <img src="<?=$caminhoClientes?>cliente-02.png" class="img-fluid rounded" alt="Cliente - 02" />
                    </div>
                    <div>
                        <img src="<?=$caminhoClientes?>cliente-03.png" class="img-fluid rounded" alt="Cliente - 03" />
                    </div>
                    <div>
                        <img src="<?=$caminhoClientes?>cliente-04.png" class="img-fluid rounded" alt="Cliente - 04" />
                    </div>
                    <div>
                        <img src="<?=$caminhoClientes?>cliente-01.png" class="img-fluid rounded" alt="Cliente - 01" />
                    </div>
                    <div>
                        <img src="<?=$caminhoClientes?>cliente-02.png" class="img-fluid rounded" alt="Cliente - 02" />
                    </div>
                    <div>
                        <img src="<?=$caminhoClientes?>cliente-03.png" class="img-fluid rounded" alt="Cliente - 03" />
                    </div>
                    <div>
                        <img src="<?=$caminhoClientes?>cliente-04.png" class="img-fluid rounded" alt="Cliente - 04" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php /* DISTRIBUIDOR - PARCEIROS - CLIENTES */?>