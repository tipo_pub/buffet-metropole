
<div id="ssb-container" class="ssb-btns-right ssb-anim-icons">
    <ul class="ssb-dark-hover">
	    <li id="ssb-btn-0">
            <p>
                <a href="<?=$whatslink;?>" target="_blank"><span class="text-color-light fab fa-whatsapp"></span></a>
            </p>
        </li>
		 <li id="ssb-btn-2">
            <p>
                <a href="mailto:<?=$email?>" title="<?=$email?>"><span class="text-color-light fa fa-envelope"></span></a>
            </p>
        </li>
	</ul>
</div>
