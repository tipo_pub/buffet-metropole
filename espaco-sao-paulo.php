<?php
$title = 'Espaço São Paulo';
$description = '';
$keywords = '';
?>
<?php require_once 'includes/head.php'; ?>
<?php require_once 'includes/header.php'; ?>
<section id="espaco-sp">
	<div class="container">
		<div class="row">
			<div class="col-12 p-lg-5 text-center pt-3">
				<div class="content-espaco">
					<h1 class="text-primary text-12 font-GreatVibes">Espaço São Paulo</h1>
					<p>
						O <strong>Espaço São Paulo</strong> é ideal para sua festa de casamento, 15 anos ou bodas e, possui tudo que é necessário para sua festa se tornar inesquecível.
					</p>
					<p>
						O <strong>Espaço São Paulo</strong> possui capacidade para até 250 pessoas. O ambiente possui sistema central de ar condicionado, cozinha própria e toda infra estrutura de som e iluminação necessária para seu evento ser um verdadeiro sucesso.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12"> 
				<h2 class="text-primary text-12 font-GreatVibes text-lg-center">Características</h2>
			</div>

			<div class="col-lg-6 pb-3">
				<ul>
					<li>Capacidade para 250 pessoas;</li>
					<li>Ar acondicionado central;</li>
					<li>Cozinha exclusiva;</li>
					<li>Som e iluminação controlados por computador;</li>
					<li>Serviço de Vallet;</li>
				</ul>
			</div>
			<div class="col-lg-6 pb-3">
				<ul>
					<li>Seguranças treinados e CFTV;</li>
					<li>Recepcionistas treinadas;</li>
					<li>Estruturas Box Truss para iluminação;</li>
					<li>Equipe operacional capacitada e uniformizada.</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section id="galeria-espaco" class="section-dark">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 p-4">
				<h4 class="font-weight-bold text-primary text-12 font-GreatVibes text-center pt-4">Galeria</h4>
			</div>
			<article class="post post-large blog-single-post border-0 m-0 p-0">

				<div class="post-image ml-0">
					<div class="lightbox" data-plugin-options="{'delegate': 'a', 'type': 'image', 'gallery': {'enabled': true}, 'mainClass': 'mfp-with-zoom', 'zoom': {'enabled': true, 'duration': 300}}">
						<div class="row mx-0">

							<!-- Loop images gallery -->
							<?php 	
							$i = 1;
							$dir = count(glob("img/galeria/espacos/sao-paulo/*.jpg"));
								// echo $dir;
							while ($i <= $dir):
								?>
								<div class="col-6 col-md-4 p-0">
									<a href="<?=$caminhoEspacos . 'sao-paulo/' . $i . '.jpg'; ?>">
										<span class="thumb-info thumb-info-no-borders thumb-info-centered-icons">
											<span class="thumb-info-wrapper">
												<img src="<?=$caminhoEspacos . 'sao-paulo/thumbs/' . $i . '.jpg';?>" class="img-fluid" loading="lazy"/>
												<span class="thumb-info-action">
													<span class="thumb-info-action-icon thumb-info-action-icon-light"><i class="fas fa-plus text-dark"></i></span>
												</span>
											</span>
										</span>
									</a>
								</div>
							<?php
								$i++; 
							endwhile;
							?>
						</div>
					</div>
				</div>
			</article>
		</div>
	</div>
</div>
</div>
</section>

<section id="espacos" class="section-dark border-0 pb-3">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center pt-5 pb-lg-5">
				<h3 class="font-weight-bold text-primary text-12">Outros Espaços</h3>
			</div>
			<div class="col-12 col-lg-6 align-self-center" >
				<h4 class="font-weight-normal text-10 text-brown font-GreatVibes">
					Espaço Paris
				</h4>
				<ul>
					<li>Capacidade para até 400 convidados;</li>
					<li>Salão principal retangular com 350 m² de área;</li>
					<li>Charmoso terraço jardim;</li>
					<li>Salão receptivo com 50 m²;</li>
					<li>Elevador com capacidade para até 9 pessoas;</li>
					<li>Ar condicionado central e sala exclusiva para Noiva ou Debutante.
					</li>

				</ul>
				<a href="<?=$url ?>espaco-paris" class="link-espaco">Para mais informações clique aqui</a>
					<!-- </div>	
						<div class="col-12 col-lg-8 pb-lg-3"> -->
							<div class="slider-espaco pt-3">
								<?php  
								$i = 1;
						// while($i <= 1):
								$imgEspaco =  $pastaEspaco. 'paris/'. $i . '.jpg';

								echo '<img data-lazy="'.$imgEspaco.'" alt="Espaço Paris" loading="lazy">';

						// 	$i++;
						// endwhile;
								?>

							</div>
						</div>

						<div class="col-12 col-lg-6">
							<h4 class="font-weight-normal text-10 text-brown font-GreatVibes">
								Espaço New York
							</h4>
							<ul>
								<li>Capacidade para até 350 convidados;</li>
								<li>Ar condicionado central;</li>
								<li>Cozinha exclusiva;</li>
								<li>Controle de som e iluminação por computador;</li>
								<li>Serviço de Vallet;</li>
								<li>Sala exclusiva para Noiva ou Debutante.
								</li>
							</ul>
							<a href="<?=$url ?>espaco-new-york" class="link-espaco">Para mais informações clique aqui</a>
						<!-- </div>	
							<div class="col-12 col-lg-8 pb-lg-3"> -->
								<div class="slider-espaco pt-3">
									<?php  
									$i = 1;
									while($i <= 3):
										$imgEspaco =  $pastaEspaco. 'new-york/'. $i . '.jpg';
										echo '<img data-lazy="'.$imgEspaco.'" alt="Espaço São Paulo" loading="lazy">';
										$i++;
									endwhile;
									?>
								</div>
							</div>
						</div>
					</div>
				</section>



<section id="localizacao">
	<div class="col-12 p-4">
		<h4 class="font-weight-bold text-primary text-12 font-GreatVibes text-center pt-4">Localização</h4>
	</div>
	<div class="mapouter">
		<div class="gmap_canvas">
			<iframe width="100%" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=Avenida%20Ordem%20e%20Progresso%2C%201085%20-%201%C2%BA%20andar%2C%20S%C3%A3o%20Paulo%20-%20SP.&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
		</div>
	</div>
</section>

<?php 
require 'includes/footer.php';
?>