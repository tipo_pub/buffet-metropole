<?php
if(!empty($menuTopo)){
	$classe_dropdown = 'dropdown';
	$classe_item = $classe_dropdown.'-item';
	$classe_toggle = $classe_dropdown.'-toggle';
	$classe_submenu =  $classe_dropdown.'-menu';
	$LI_mapa_site = false;
} elseif(!empty($menuRodape)) {
	$classe_dropdown = '';
	$classe_item = 'text-4 link-hover-style-1';
	$classe_toggle = '';
	$classe_submenu =  '';
	$LI_mapa_site = true;
	$submenu_mapa_site = false;
} elseif(!empty($menuMapaSite)) {
	$classe_dropdown = '';
	$classe_item = '';
	$classe_toggle = '';
	$classe_submenu =  '';
	$LI_mapa_site = false;
	$submenu_mapa_site = true;
}
?>

<li><a href="<?=$url;?>" class="<?=$classe_item?>" title="Home">Home</a></li>
<li><a href="<?=$url;?>institucional" class="<?=$classe_item?>" title="Institucional">Institucional</a></li>
<li class="<?=$classe_dropdown?>"><a href="espacos" class="<?=$classe_item.' '.$classe_toggle?>" title="Espaços">Espaços</a>
	<ul class="<?=$classe_submenu?>">
		<li><a href="<?=$url;?>espaco-sao-paulo" class="<?=$classe_item?>" title="Espaço São Paulo">Espaço São Paulo</a></li>
		<li><a href="<?=$url;?>espaco-new-york" class="<?=$classe_item?>" title="Espaço New York">Espaço New York</a></li>
		<li><a href="<?=$url;?>espaco-paris" class="<?=$classe_item?>" title="Espaço Paris">Espaço Paris</a></li>
	</ul>
</li>
<li><a href="<?=$url;?>depoimentos" class="<?=$classe_item?>" title="Depoimentos">Depoimentos</a></li>
<li><a href="<?=$url;?>blog" target="_blank" class="<?=$classe_item?>" title="Blog">Blog</a></li>
<?php if(empty($menuRodape)) {?>
<li><a href="<?=$url;?>galeria" class="<?=$classe_item?>" title="Galeria">Galeria</a></li>
<li class="<?=$classe_dropdown?>"><a href="<?=$url;?>contato" class="<?=$classe_item.' '.$classe_toggle?>" title="Contato">Contato</a>
	<ul class="<?=$classe_submenu?>">
		<li><a href="<?=$url;?>trabalhe-conosco" class="<?=$classe_item?>" title="Trabalhe Conosco">Trabalhe Conosco</a></li>
	</ul>
</li>
<?php } if(!empty($LI_mapa_site)) {?>
</ul>
<ul class="lista-rodape">
	<li><a href="<?=$url;?>galeria" class="<?=$classe_item?>" title="Galeria">Galeria</a></li>
	<li><a href="<?=$url;?>contato" class="<?=$classe_item?>" title="Contato">Contato</a></li>
	<li><a href="<?=$url;?>mapa-site" class="<?=$classe_item?>" title="Mapa do Site">Mapa do Site</a></li>
	<?php } elseif(!empty($submenu_mapa_site)) {?>
</ul>
<ul class="lista-mapa-site">
	<li><a href="<?=$url;?>mapa-site" class="<?=$classe_item?>" title="Outros Produtos">Outros Produtos</a>
		<ul>

				<?php
			$keys = array_rand($menu, count($menu));

			$i = 0;
			foreach ($keys as $link) {
				$palavra = $menu[$link];
				echo "<li><a href=\"$url$link\" title=\"$palavra\">$palavra</a></li>";

				$i++;
			}
			?>
		</ul>
	</li>
	<?php }?>