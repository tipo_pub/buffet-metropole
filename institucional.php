<?php
$title = 'Institucional';
$description = '';
$keywords = '';
?>
<?php require_once 'includes/head.php'; ?>
<?php require_once 'includes/header.php'; ?>
<section id="institucional">

	<div class="container">
		<div class="row">
			
			<div class="col-12 col-lg-6 pt-4 pb-5">
				<div class="slider-institucional">

					<img data-lazy="<?=$pastaImg?>/institucional/espaco-sao-paulo.jpg" class="img-fluid" alt="Buffet São Paulo" />

					<img data-lazy="<?=$pastaImg?>/institucional/espaco-new-york.jpg" class="img-fluid" alt="Buffet New York" />

					<img data-lazy="<?=$pastaImg?>/institucional/espaco-paris.jpg" class="img-fluid" alt="Buffet Paris" />
				</div>
			</div>

			<div class="col-12 col-lg-6 pt-5 text-center text-lg-left">
				<h1 class="text-primary text-12 font-GreatVibes">Sobre o Metropole</h1>
				<div class="desc-content pb-3">
					<p>O <strong>Metropole </strong>é um buffet com estrutura completa para eventos.</p>
					<p>Somos especializados em <strong>Festas Debutantes, </strong>bem como, <strong> Casamentos, Bodas, Corporativos e Formaturas.</strong></p>
					<p>Possuímos <strong>localização privilegiada</strong> na cidade de São Paulo para <strong> fazer seu sonho acontecer.</strong></p>
					<p>Sua satisfação é nosso maior patrimônio, o bem mais valioso que uma empresa do ramo pode ter!</p>
				</div>
			</div>
			<div class="col-6 col-lg-3 pb-3 text-center topic-institucional">
				<i class="fab fa-fort-awesome"></i>

				<h2 class="font-weight-normal text-7 font-GreatVibes ">Estrutura <strong>Completa</strong></h2>
				<p>
					Possuímos <strong>3 espaços</strong> para festas com capacidade para até <strong>400 pessoas</strong> e com <strong>infraestrutura completa</strong> e <strong>serviços de alta qualidade.</strong>
				</p>
			</div>
			<div class="col-6 col-lg-3 pb-3 text-center topic-institucional">
				<i class="fas fa-map-marker-alt"></i>

				<h2 class="font-weight-normal text-7 font-GreatVibes">Localização <strong>Privilegiada</strong></h2>
				<p>
					Possuímos <strong>3 espaços</strong> para festas com capacidade para até <strong>400 pessoas</strong> e com <strong>infraestrutura completa</strong> e <strong>serviços de alta qualidade.</strong>
				</p>
			</div>
			<div class="col-6 col-lg-3 pb-3 text-center topic-institucional">
				<i class="fas fa-user-tie"></i>
				<h2 class="font-weight-normal text-7 font-GreatVibes">
					Assessoria <strong>Profissional</strong></h2>
					<p>
						Possuímos <strong>3 espaços</strong> para festas com capacidade para até <strong>400 pessoas</strong> e com <strong>infraestrutura completa</strong> e <strong>serviços de alta qualidade.</strong>
					</p>
				</div>
				<div class="col-6 col-lg-3 pb-3 text-center topic-institucional">
					<i class="fas fa-utensils"></i>
					<h2 class="font-weight-normal text-7 font-GreatVibes"><strong>Gastronomia</strong></h2>
					<p>
						Possuímos <strong>3 espaços</strong> para festas com capacidade para até <strong>400 pessoas</strong> e com <strong>infraestrutura completa</strong> e <strong>serviços de alta qualidade.</strong>
					</p>
				</div>
			</div>	
		</div>
	</section>
	
<?php include 'includes/espacos.php'; ?>
		<?php 
		require 'includes/section-galeria.php';
		?>

		<section id="localizacao">
			<div class="col-12 p-4">
				<h4 class="font-weight-bold text-primary text-12 font-GreatVibes text-center pt-4">Localização</h4>
			</div>
			<div class="mapouter">
				<div class="gmap_canvas">
					<iframe width="100%" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=Avenida%20%20Ordem%20e%20Progresso%2C%201085%20-%201%C2%BA%20andar%2C%20S%C3%A3o%20Paulo%20-%20SP.&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
				</div>
			</div>
		</section>
		<?php
		require 'includes/footer.php';
		?>