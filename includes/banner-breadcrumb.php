<?php if(isset($linkPagina) && ($linkPagina == 'index') || ($linkPagina == ''))  {?>

	<div class="slider-container rev_slider_wrapper" style="height: 50vh;">
		<div id="revolutionSlider" class="slider rev_slider slider" data-version="5.4.8" data-plugin-revolution-slider data-plugin-options="{'sliderLayout': 'auto', 'delay': 100000, 'responsiveLevels': [4096,1600,992,500], 'gridwidth': [1140, 0, 0, 1600], 'gridheight': [810, 630, 1000, 1000]}">
			<ul>
				<?php /**/?>
				<!-- Espaço São Paulo -->

				<li class="slide-overlay slide-overlay-opacity" data-transition="fade">
					<img src="<?=$pastaImg?>espaco-new-york.jpg" alt="Espaço New York" data-bgposition="center center"  data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" />

					<div class="tp-caption text-color-light font-weight-normal font-Parisienne textsm-slide" data-x="center" data-y="center" data-voffset="['-80','-80','-80','-130']" data-start="700" data-fontsize="['16','16','16','80']" data-lineheight="['120','120','120','120']" data-transform_in="y:[-50%];opacity:0;s:500;">Buffet</div>

					<div class="tp-caption font-weight-extra-bold text-color-light word-slider" data-frames='[{"delay":1000,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['-290','-290','-290','-290']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">M</div>

					<div class="tp-caption font-weight-extra-bold text-color-light word-slider" data-frames='[{"delay":1100,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['-200','-200','-200','-200']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">E</div>

					<div class="tp-caption font-weight-extra-bold text-color-light word-slider" data-frames='[{"delay":1200,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['-125','-125','-125','-125']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">T</div>

					<div class="tp-caption font-weight-extra-bold text-color-light word-slider" data-frames='[{"delay":1300,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['-45','-45','-45','-45']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">R</div>

					<div class="tp-caption font-weight-extra-bold text-color-light word-slider" data-frames='[{"delay":1400,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['45','45','45','45']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">O</div>

					<div class="tp-caption font-weight-extra-bold text-color-light word-slider" data-frames='[{"delay":1500,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['134','134','134','134']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">P</div>

					<div class="tp-caption font-weight-extra-bold text-color-light word-slider" data-frames='[{"delay":1600,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['225','225','225','225']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">O</div>

					<div class="tp-caption font-weight-extra-bold text-color-light word-slider" data-frames='[{"delay":1700,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['310','310','310','310']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">L</div>

					<div class="tp-caption font-weight-extra-bold text-color-light word-slider" data-frames='[{"delay":1800,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['378','378','378','378']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">E</div>
				</li>

				<li class="slide-overlay slide-overlay-opacity" data-transition="fade">
					
					<img src="<?=$pastaImg?>espaco-sao-paulo.jpg" alt="Espaço São Paulo" data-bgposition="center center"  data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" />
					
					<div class="tp-caption text-color-light font-weight-normal font-Parisienne textsm-slide" data-x="center" data-y="center" data-voffset="['-80','-80','-80','-130']" data-start="700" data-fontsize="['16','16','16','40']" data-lineheight="['25','25','25','45']" data-transform_in="y:[-50%];opacity:0;s:500;">Espaço para</div>

					<div class="tp-caption font-weight-extra-bold text-color-light" data-frames='[{"delay":1000,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['-390','-390','-390','-390']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">C</div>

					<div class="tp-caption font-weight-extra-bold text-color-light" data-frames='[{"delay":1100,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['-300','-300','-300','-300']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">A</div>

					<div class="tp-caption font-weight-extra-bold text-color-light" data-frames='[{"delay":1200,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['-210','-210','-210','-210']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">S</div>

					<div class="tp-caption font-weight-extra-bold text-color-light" data-frames='[{"delay":1300,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['-125','-125','-125','-125']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">A</div>

					<div class="tp-caption font-weight-extra-bold text-color-light" data-frames='[{"delay":1400,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['-10','-10','-10','-10']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">M</div>

					<div class="tp-caption font-weight-extra-bold text-color-light" data-frames='[{"delay":1500,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['80','80','80','80']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">E</div>

					<div class="tp-caption font-weight-extra-bold text-color-light" data-frames='[{"delay":1600,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['165','165','165','165']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">N</div>

					<div class="tp-caption font-weight-extra-bold text-color-light" data-frames='[{"delay":1700,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['255','255','255','255']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">T</div>

					<div class="tp-caption font-weight-extra-bold text-color-light" data-frames='[{"delay":1800,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['340','340','340','340']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">O</div>

					<div class="tp-caption font-weight-extra-bold text-color-light" data-frames='[{"delay":1900,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['425','425','425','425']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">S</div>
				</li>

				<li class="slide-overlay slide-overlay-opacity" data-transition="fade">
					<img src="<?=$pastaImg?>espaco-paris.jpg" alt="Espaço Paris" data-bgposition="center center"  data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" />

					<div class="tp-caption text-color-light font-weight-normal font-Parisienne textsm-slide" data-x="center" data-y="center" data-voffset="['-80','-80','-80','-130']" data-start="700" data-fontsize="['16','16','16','40']" data-lineheight="['25','25','25','45']" data-transform_in="y:[-50%];opacity:0;s:500;">Eventos</div>

					<div class="tp-caption font-weight-extra-bold text-color-light" data-frames='[{"delay":1000,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['-470','-470','-470','-470']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">C</div>

					<div class="tp-caption font-weight-extra-bold text-color-light" data-frames='[{"delay":1100,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['-380','-380','-380','-380']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">O</div>

					<div class="tp-caption font-weight-extra-bold text-color-light" data-frames='[{"delay":1200,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['-290','-290','-290','-290']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">R</div>

					<div class="tp-caption font-weight-extra-bold text-color-light" data-frames='[{"delay":1300,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['-200','-200','-200','-200']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">P</div>

					<div class="tp-caption font-weight-extra-bold text-color-light" data-frames='[{"delay":1400,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['-110','-110','-110','-110']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">O</div>

					<div class="tp-caption font-weight-extra-bold text-color-light" data-frames='[{"delay":1500,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['-20','-20','-20','-20']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">R</div>

					<div class="tp-caption font-weight-extra-bold text-color-light" data-frames='[{"delay":1600,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['90','90','90','90']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">A</div>

					<div class="tp-caption font-weight-extra-bold text-color-light" data-frames='[{"delay":1700,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['160','160','160','160']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">T</div>

					<div class="tp-caption font-weight-extra-bold text-color-light" data-frames='[{"delay":1800,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['220','220','220','220']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">I</div>

					<div class="tp-caption font-weight-extra-bold text-color-light" data-frames='[{"delay":1900,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['290','290','290','290']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">V</div>

					<div class="tp-caption font-weight-extra-bold text-color-light" data-frames='[{"delay":2000,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['380','380','380','380']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">O</div>

					<div class="tp-caption font-weight-extra-bold text-color-light" data-frames='[{"delay":2100,"speed":1000,"frame":"0","from":"opacity:0;x:-50%;","to":"opacity:0.7;x:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]' data-x="center" data-hoffset="['465','465','465','465']" data-y="center" data-fontsize="['145','145','145','100']" data-lineheight="['150','150','150','260']">S</div>
				</li>


			</ul>
		</div>
	</div>
	<? ?>
	<?php 
}else if ($linkPagina == 'contato') : ?>
	<section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-dark overlay-show overlay-op-7" style="background-image: url(<?php echo $caminhoBanners;?>banner-02.jpg);">
		<div class="container">
			<div class="row mt-5">
				<div class="col-md-12 align-self-center p-static order-1 text-center">
					<h1 class="font-Pattaya"><?=$title?></h1>
				</div>
				<div class="col-md-12 align-self-center order-2 mt-4">
					<ul class="breadcrumb breadcrumb-light d-block text-center">
						<?=breadcrumb()?>
					</ul>
				</div>
			</div>
		</div>
	</section>

<?php elseif ($linkPagina == 'espaco-sao-paulo'): ?>

	<section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-dark overlay-show overlay-op-7" style="background-image: url(<?php echo $caminhoBanners;?>banner-06.jpg);">
		<div class="container">
			<div class="row mt-5">
				<div class="col-md-12 align-self-center p-static order-1 text-center">
					<h1 class="font-Pattaya"><?=$title?></h1>
				</div>
				<div class="col-md-12 align-self-center order-2 mt-4">
					<ul class="breadcrumb breadcrumb-light d-block text-center">
						<?=breadcrumb()?>
					</ul>
				</div>
			</div>
		</div>
	</section>


<?php elseif ($linkPagina == 'espaco-new-york'): ?>

	<section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-dark overlay-show overlay-op-7" style="background-image: url(<?php echo $caminhoBanners;?>banner-05.jpg);">
		<div class="container">
			<div class="row mt-5">
				<div class="col-md-12 align-self-center p-static order-1 text-center">
					<h1 class="font-Pattaya"><?=$title?></h1>
				</div>
				<div class="col-md-12 align-self-center order-2 mt-4">
					<ul class="breadcrumb breadcrumb-light d-block text-center">
						<?=breadcrumb()?>
					</ul>
				</div>
			</div>
		</div>
	</section>

<?php elseif ($linkPagina == 'espaco-paris'): ?>

	<section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-dark overlay-show overlay-op-7" style="background-image: url(<?php echo $caminhoBanners;?>banner-04.jpg);">
		<div class="container">
			<div class="row mt-5">
				<div class="col-md-12 align-self-center p-static order-1 text-center">
					<h1 class="font-Pattaya"><?=$title?></h1>
				</div>
				<div class="col-md-12 align-self-center order-2 mt-4">
					<ul class="breadcrumb breadcrumb-light d-block text-center">
						<?=breadcrumb()?>
					</ul>
				</div>
			</div>
		</div>
	</section>


<?php else: ?>

	<section class="page-header page-header-modern page-header-background page-header-background-md overlay overlay-color-dark overlay-show overlay-op-7" style="background-image: url(<?php echo $caminhoBanners;?>banner-01.jpg);">
		<div class="container">
			<div class="row mt-5">
				<div class="col-md-12 align-self-center p-static order-1 text-center">
					<h1 class="font-Pattaya"><?=$title?></h1>
				</div>
				<div class="col-md-12 align-self-center order-2 mt-4">
					<ul class="breadcrumb breadcrumb-light d-block text-center">
						<?=breadcrumb()?>
					</ul>
				</div>
			</div>
		</div>
	</section> 
	

<?php endif; ?>