function loadSliders(){
	$('.slider-institucional').slick({
		fade: true,
		arrows: false,
		autoplay: true,
		infinite: true,
		lazyLoad:'ondemand'
	});

	$('.slider-espaco').slick({
		fade: true,
		arrows: false,
		autoplay: true,
		infinite: true,
		lazyLoad:'ondemand'
	});

}

