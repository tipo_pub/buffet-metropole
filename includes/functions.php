<?php

//Trata Letras Removendo acentos
function tratar_letras($string)
{
    $conversao = array('á' => 'a',
        'à' => 'a',
        'ã' => 'a',
        'â' => 'a',
        'é' => 'e',
        'ê' => 'e',
        'í' => 'i',
        'ï' => 'i',
        'ó' => 'o',
        'ô' => 'o',
        'õ' => 'o',
        "ö" => "o",
        'ú' => 'u',
        'ü' => 'u',
        'ç' => 'c',
        'ñ' => 'n',
        'Á' => 'A',
        'À' => 'A',
        'Ã' => 'A',
        'Â' => 'A',
        'É' => 'E',
        'Ê' => 'E',
        'Í' => 'I',
        'Ï' => 'I',
        "Ö" => "O",
        'Ó' => 'O',
        'Ô' => 'O',
        'Õ' => 'O',
        'Ú' => 'U',
        'Ü' => 'U',
        'Ç' => 'C',
        'N' => 'Ñ',
        'ý' => 'y',
        'ÿ' => 'y',
        'Ý' => 'Y',
    );
    foreach ($conversao as $key => $value) {
        $string = str_replace($key, $value, $string);
    }
    return $string;
}

## Arquivos Inline - CSS e JS
function arquivos_inline($arquivo)
{
    if (file_exists($arquivo)) {
        $extensaoArquivo = explode('.', $arquivo);
        $extensaoArquivo = end($extensaoArquivo);
        $extensaoArquivo = strtolower($extensaoArquivo);
        if ("css" == $extensaoArquivo) {
            $retornaCodigo = file_get_contents($arquivo);
            $retornaCodigo = str_replace(array("\n","\r","\t","\n\t"), "", $retornaCodigo);
            $retornaCodigo = str_replace(": ", ":", $retornaCodigo);
            $retornaCodigo = str_replace(", ", ",", $retornaCodigo);
            $retornaCodigo = str_replace("; ", ";", $retornaCodigo);
            $retornaCodigo = str_replace(";   ", ";", $retornaCodigo);
            $retornaCodigo = str_replace("{ ", "{", $retornaCodigo);
            $retornaCodigo = str_replace(" {", "{", $retornaCodigo);
            $retornaCodigo = str_replace(" { ", "{", $retornaCodigo);
            $retornaCodigo = str_replace("   {", "{", $retornaCodigo);
            $retornaCodigo = str_replace("} ", "}", $retornaCodigo);
            $retornaCodigo = str_replace(" }", "}", $retornaCodigo);
            $retornaCodigo = str_replace(" } ", "}", $retornaCodigo);
            $expressaoCSS = '!/\*[^*]*\*+([^/][^*]*\*+)*/!';
            $retornaCodigo = preg_replace($expressaoCSS, '', $retornaCodigo);
            return $retornaCodigo;
        } elseif ("js" == $extensaoArquivo) {
            $retornaCodigo = file_get_contents($arquivo);
            $retornaCodigo = str_replace(array("\n","\t","\r","\n\t"), '', $retornaCodigo);
            $expressaoJS = '/(?:(?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:(?<!\:|\\\|\')\/\/.*))/';
            $retornaCodigo = preg_replace($expressaoJS, '', $retornaCodigo);

            return $retornaCodigo;
        }
    }
}

## Mostrar Tags Randômicas nas Palavras-Chave
function montarTags($menu, $qntTags = 30)
{
    global $url;
    global $qntTags;
    $retorno = "";
    $mostrarTags = array_rand($menu, $qntTags);
    if (isset($mostrarTags) && count($mostrarTags) > 0) {
        foreach ($mostrarTags as $linkPagina) {
            $palavraChave = $menu[$linkPagina];
            $retorno .= '<li><a href="' . $url . $linkPagina . '" title="' . $palavraChave . '">' . $palavraChave . '</a></li>';
        }
        print_r ($retorno);
    }
}

## Mostrar Menu das Palavras-Chave
function montarMenu($menu)
{
    global $url;
    $retorno = "";
    $countMenu = (is_array($menu) ? count($menu) : 0);
    $mostrarMenu = array_rand($menu, $countMenu);
    if (isset($mostrarMenu) && count($mostrarMenu) > 0) {
        foreach ($mostrarMenu as $linkPagina) {
            $palavraChave = $menu[$linkPagina];
            $retorno .= '<li><a href="' . $url . $linkPagina . '" title="' . $palavraChave . '">' . $palavraChave . '</a></li>';
        }
        print_r ($retorno);
    }
}

## Mostrar Thumbs Randômicos no Palavras-Chave
function mostrarCarrossel($menu,$qntThumbsCarrossel = 20) {
    global $url;
    global $caminhoThumbs;
    global $qntThumbsCarrossel;
    $retorno="";
    $mostrarThumbsCarrossel=array_rand($menu,$qntThumbsCarrossel);
    if(isset($mostrarThumbsCarrossel) && count($mostrarThumbsCarrossel)>0){
        foreach($mostrarThumbsCarrossel as $linkPagina)
        {
            $palavraChave=$menu[$linkPagina];
            $retorno.='<div class="team-member">';
            $retorno.='<div class="team-image">';
            $retorno.='<a href="'.$url.$linkPagina.'" title="'.$palavraChave.'">';
            $retorno.='<img src="'.$caminhoThumbs.$linkPagina.'.jpg" alt="'.$palavraChave.'" title="'.$palavraChave.'" />';
            $retorno.='</a>';
            $retorno.='</div>';
            $retorno.='<div class="team-desc">';
            $retorno.='<a href="'.$url.$linkPagina.'" title="'.$palavraChave.'">';
            $retorno.='<h3>'.$palavraChave.'</h3>';
            $retorno.='</a>';
            $retorno.='</div>';
            $retorno.='</div>';
        }
        print_r($retorno);
    }
}


## Ordenar Array (Sem Levar em Consideração Maiusculos e minusculos)
function acasesort($array,$array2=array("vazia"))
{
    foreach ($array as $itemKey => $itemValue) {
        $itemValue = tratar_letras($itemValue);
        $arrayLower[$itemKey] = mb_strtolower($itemValue, 'UTF-8');
    }
    asort($arrayLower);
    if (isset($array2[0]) && "vazia" == $array2[0]) {
        foreach ($arrayLower as $arrayLowerItemKey => $arrayLowerItemValue) {
            $arrayLower[$arrayLowerItemKey] = $array[$arrayLowerItemKey];
        }
        return $arrayLower;
    } else if (isset($array2) && count($array2) > 0) {
        $arrayNova=array();
        foreach ($arrayLower as $arrayLowerItemKey => $arrayLowerItemValue) {
            if (in_array($arrayLowerItemKey, $array2)) {
                array_push($arrayNova, $arrayLowerItemKey);
            }
        }
        if (isset($arrayNova) && count($arrayNova) > 0) {
            return $arrayNova;
        } else {
            return asort($array2);
        }
    }
}

## Verificação de Existência das Páginas
function keysExist()
{
    global $url;
    global $menu;
    $i = 0;
    $qtdMenu = count($menu);
    foreach ($menu as $key => $value) {
        if (!file_exists($key . '.php') && !file_exists($key)) {
            $i++;
        }
    }
    if ($i == $qtdMenu) {
        return false;
    } else {return true;}
}

## Validação de Telefone
function telefone($tel, $Param_ddd = false) {
    global $ddd;
    global $nomeEmpresa;
    global $prestadora;
    $dddLink = ($Param_ddd) ? $Param_ddd : $ddd;
    $tel = str_replace(array('-', ' ', '(', ')', '_', '*', '.'), '', $tel);
    $dddLink = str_replace(array('(',")"), '', $dddLink);
    $prestadora = str_replace(array('(',")"), '', $prestadora);
    $tel =  preg_replace('/[^0-9]/', '', $tel);
    $countTel = (is_array($tel) ? count($tel) : 0);
    $foneLink = '<a href="tel:'.$prestadora.$dddLink.$tel.'" title="Ligue para '.$nomeEmpresa.'"><i class="fa fa-phone"></i> <span>'.$ddd.' '.trim(substr($tel, 0, ($countTel-4)).'-'.substr($tel, ($countTel-4))).'</span></a>';
    return $foneLink;
}

## Mostrar Breadcrumb
function breadcrumb(){
    global $title;
    global $menu;
    global $urlPagina;
    global $linkPagina;
    global $urlAtividadesEmpresa;
    global $AtividadesEmpresa;
    global $canonical;
    global $url;
    
    $breadcrumb = '';
    
    $breadcrumb .= '<li><a href="'.$url.'" title="Voltar a página inicial">Home</a></li>';
    if(isset($menu) && (array_key_exists($urlPagina,$menu) || array_key_exists($linkPagina,$menu))) {
        $breadcrumb .= '<li><a href="'.$url.$urlAtividadesEmpresa.'" title="'.$AtividadesEmpresa.'">'.$AtividadesEmpresa.'</a></li>';
    }
    $breadcrumb .= '<li class="active"><a href="'.$canonical.'" tite="'.$title.'">'.$title.'</a></li>';
    
    return $breadcrumb;
}

## Mostrar Tags
// function montarTags($key) {
//     if(isset($key) && !empty($key) && is_string($key)) {
//         $tagArr = explode(',',$key);
//         $tagArr = array_slice($tagArr, 0, 5);
//         $tags = implode(', ',$tagArr);
//         return $tags;
//     }
//     return '';
// }