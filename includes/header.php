</head>
<body>
	<div class="body">
		<div role="main" class="main">
			<?php include "includes/banner-breadcrumb.php";?>
			<div id="intro">
				<header id="header" class="header-narrow header-below-slider" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAtElement': '#header', 'stickySetTop': '0', 'stickyChangeLogo': false}">
					<div class="header-body border-top-0 bg-dark header-body-bottom-border-fixed">
						<?php include 'includes/top-bar.php';?>
						<div class="header-container container my-0 py-0">
							<div class="header-row">
								<div class="header-column py-2">
									<div class="header-row">
										<div class="header-logo">
											<a href="<?=$url;?>">
												<img alt="<?=$nomeEmpresa;?>" src="<?=$logo;?>">
											</a>
										</div>
									</div>
								</div>
								<div class="header-column justify-content-end">
									<div class="header-row">
										<div class="header-nav header-nav-links header-nav-dropdowns-dark header-nav-light-text order-2 order-lg-1">
											<div class="header-nav-main header-nav-main-mobile-dark header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-2 header-nav-main-sub-effect-1">
												<nav class="collapse">
													<ul class="nav nav-pills" id="mainNav">
														<?php $menuTopo = true; $menuRodape = false; include 'includes/menu.php';?>
													</ul>
												</nav>
											</div>
											<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
												<i class="fas fa-bars"></i>
											</button>
										</div>                                
									</div>
								</div>
							</div>
						</div>
					</div>
				</header>
			</div>