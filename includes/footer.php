<footer id="footer" class="mt-0">
	<div class="container">
		<div class="row py-5">
			<div class="col-md-6 col-lg-4 mb-5 mb-lg-0">
				<h5 class="text-8 text-transform-none font-GreatVibes ls-0 mb-3">Menu</h5>
				<ul class="lista-rodape">
					<?php $menuRodape = true; $menuTopo = false; include "includes/menu.php";?>
				</ul>
			</div>

			<div class="col-md-6 col-lg-4 mb-5 mb-lg-0">
				<h5 class="text-8 text-transform-none font-GreatVibes ls-0 mb-3">Contatos </h5>

				<?php
				echo isset($linkHorario) && ($linkHorario != '') ? '<p class="text-4 mb-0"><a target="_blank" href="'.$linkHorario.'" title="Veja a '.$nomeEmpresa.' no Google Maps">'.$endereco.' <br /> '.$cidade.' - CEP: '.$cep.'</a></p>' : '';
				echo isset($tel) && ($tel != '') ? '<p class="text-4 mb-0 pt-0"><i class="fas fa-phone icone-invertido"></i> <a href="'.$tellink.'" title="Telefone para Contato">'.$ddd.' '.$tel.'</a></p>' : '';
				echo isset($tel2) && ($tel2 != '') ? '<p class="text-4 mb-0 pt-0"><i class="fas fa-phone icone-invertido"></i> <a href="'.$tel2link.'" title="Telefone para Contato">'.$ddd.' '.$tel2.'</a></p>' : '';
				echo isset($whats) && ($whats != '') ? '<p class="text-4 mb-0 pt-0"><i class="fab fa-whatsapp"></i> <a href="'.$whatslink.'" title="Atendimento Online">'.$ddd.' '.$whats.'</a></p>' : '';
				echo isset($email) && ($email != '') ? '<p class="text-4 mb-0 pt-0"><i class="fa fa-envelope"></i> <a href="mailto:'.$email.'" title="Enviar e-mail para: '.$email.'">'.$email.'</a></p>' : '';
				?>

				<ul class="footer-social-icons social-icons mt-3">
					<?php
					echo isset($linkFace) && ($linkFace != '') ? '<li class="social-icons-facebook"><a href="'.$linkFace.'" class="text-color-dark" target="_blank" title="Facebook - '.$nomeEmpresa.'"><i class="fab fa-facebook-f"></i></a></li>' : '';

					echo isset($linkInstagram) && ($linkInstagram != '') ? '<li class="ml-1 social-icons-instagram"><a href="'.$linkInstagram.'" class="text-color-dark" target="_blank" title="Instagram - '.$nomeEmpresa.'"><i class="fab fa-instagram"></i></a></li>' : '';

					echo isset($linkTwitter) && ($linkTwitter != '') ? '<li class="ml-1 social-icons-twitter"><a href="'.$linkTwitter.'" class="text-color-dark" target="_blank" title="Twitter - '.$nomeEmpresa.'"><i class="fab fa-twitter"></i></a></li>' : '';

					echo isset($linkedIn) && ($linkedIn != '') ? '<li class="ml-1 social-icons-linkedin"><a href="'.$linkedIn.'" class="text-color-dark" target="_blank" title="LinkedIn - '.$nomeEmpresa.'"><i class="fab fa-linkedin"></i></a></li>' : '';

					echo isset($linkYoutube) && ($linkYoutube != '') ? '<li class="ml-1 social-icons-youtube"><a href="'.$linkYoutube.'" class="text-color-dark" target="_blank" title="Youtube - '.$nomeEmpresa.'"><i class="fab fa-youtube"></i></a></li>' : '';
					?>
				</ul>
			<?php /*
				<div id="instafeedNoMargins"></div>
			*/?>
		</div>

		<div class="col-md-12 col-lg-4 mb-lg-0">
			<h5 class="text-8 text-transform-none font-GreatVibes ls-0 mb-3">Localização</h5>

			<iframe src="<?=$linkIframeMapa?>" class="iframeMapa" allowfullscreen></iframe>
		</div>
	</div>
</div>
<div class="footer-copyright footer-copyright-style-2">
	<div class="container py-4">
		<div class="row">
			<div class="col-md-8 mb-4 mb-lg-0">
				<p>Copyright &copy; <?php echo (date("Y") > '2019') ? "2019 - ".date("Y").'&nbsp;&nbsp;'.$nomeEmpresa : date("Y").'&nbsp;&nbsp;'.$nomeEmpresa; ?> | Todos os Direitos Reservados</p>
			</div>
			<div class="col-md-4 text-lg-right">
				<p>Desenvolvido por &nbsp;<a target="_blank" href="<?=$author?>" title="<?=$creditos?>"><img src="<?=$logoTipoPub?>" style="width:20px" alt="<?=$creditos?>"></a></p>
			</div>
		</div>
	</div>
</div>
<?php include 'includes/btn-lateral.php' ;?>
</footer>


<!-- Vendor -->


<!-- Theme Base, Components and Settings -->

<script src="dist/app.js"></script>

<!-- Current Page Vendor and Views -->

<!-- Theme Initialization Files -->

<!-- Theme Custom -->


<script src="https://www.google.com/recaptcha/api.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-72058135-63"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-72058135-63');
</script>

</body>

</html>
