<?php
$title = 'Mapa do Site';
$description = 'Navegue por todas as nossas páginas';
$keywords = 'mapa do site, mapa site, área de navegação';
include 'includes/head.php';
include 'includes/header.php';
?>
<div class="container py-4 d-table">
	<div class="heading heading-border heading-middle-border heading-middle-border-center">
		<h2 class="font-weight-bold text-primary pb-5">Links do Site</h2>
	</div>
	<ul class="lista-mapa-site">
		<?php $menuMapaSite = true; $menuTopo = false; include 'includes/menu.php';?>
	</ul>
</div>

<?php include 'includes/footer.php' ;?>
