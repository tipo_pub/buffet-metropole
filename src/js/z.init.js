
function init() {
	loadSliders();
     $('input[type="phone"]').mask('(00) 0 0000-0000');
     $('input[name="data"]').mask('00/00/0000');
}

// window load binds 
window.onload = init;

function DOMLoaded() {
    // these are not always necessary but sometimes they fuck with ya
    if (helpers.iOS) {
        document.querySelector('html').classList.add('ios');
    } else if (helpers.IE()) {
        document.querySelector('html').classList.add('ie');
    }
}

// domcontent binds 
document.addEventListener('DOMContentLoaded', DOMLoaded);