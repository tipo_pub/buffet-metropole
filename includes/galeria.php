<input type='hidden' id='current_page' />
<input type='hidden' id='show_per_page' />

<div class="row <?=$qntPag?> lightbox" id='content' data-plugin-options="{'delegate': 'a', 'type': 'image', 'gallery': {'enabled': true}}">

	<!-- Loop images gallery -->
	<?php 	
	$i = 1;
	$dir = count(glob("img/galeria/*.jpg"));
	// echo $dir;
	while ($i <= $dir):
		?>
		<div class="col-6 col-md-4 p-2">
			<a href="<?=$caminhoGaleria . $i .'.jpg'; ?>">
				<span class="thumb-info thumb-info-no-borders thumb-info-centered-icons">
					<span class="thumb-info-wrapper">
						<img src="<?=$caminhoGaleria . 'thumbs/' . $i .'.jpg'; ?>" class="img-fluid" loading="lazy"/>
						<span class="thumb-info-action">
							<span class="thumb-info-action-icon thumb-info-action-icon-light"><i class="fas fa-plus text-dark"></i></span>
						</span>
					</span>
				</span>
			</a>
		</div>
		<?php
			$i++; 
			endwhile;
		?>
</div>
<div class="d-flex align-items-center justify-content-center" id='portfolioPagination'></div>
<br>