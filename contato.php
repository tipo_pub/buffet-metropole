<?php
$title = 'Contato';
$description = '';
$keywords = '';
?>
<?php require_once 'includes/head.php'; ?>
<?php require_once 'includes/header.php'; ?>
<section id="espaco-sp">
	<div class="container">
		<div class="row">
			<div class="col-12 p-lg-5 text-center pt-3">
				<div class="content-espaco">
					<h1 class="text-primary text-12 font-GreatVibes">Contato</h1>
					<p>
						Preencha corretamente o formulário abaixo e receba uma proposta personalizada para seu evento.
					</p>
					<form action="">
						<div class="form-group">
							<input type="text" class="form-control" name="nome" placeholder="Nome*" required>
						</div>
						<div class="form-group">
							<input type="mail" class="form-control" name="email" placeholder="E-mail" required>
						</div>
						<div class="form-group">
							<input type="phone" step="1"  name="Telefone/Celular" class="form-control" placeholder="Telefone*" pattern=".{15,16}" required title="Insira um número de celular ou telefone." required>
						</div>
						<div class="form-group">
							<select class="form-control" name="" id="" required>
								<option selected disabled value="">Selecione o tipo de evento*</option>
								<option value="Casamento">Casamento</option>
								<option value="Debutantes">Debutante</option>
								<option value="Corporativo">Corporativo</option>
								<option value="Formatura">Formatura</option>
								<option value="Outros">Outros</option>
							</select>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="Como encontrou" placeholder="Onde nos encontrou?">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="data" placeholder="Data" required>
						</div>
						<button type="submit" class="btn btn-primary w-100 text-4">ENVIAR</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="galeria-espaco" class="section-dark">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 p-4">
				<h4 class="font-weight-bold text-primary text-12 font-GreatVibes text-center pt-4">Galeria</h4>
			</div>
			<article class="post post-large blog-single-post border-0 m-0 p-0">

				<div class="post-image ml-0">
					<div class="lightbox" data-plugin-options="{'delegate': 'a', 'type': 'image', 'gallery': {'enabled': true}, 'mainClass': 'mfp-with-zoom', 'zoom': {'enabled': true, 'duration': 300}}">
						<div class="row mx-0">

							<!-- Loop images gallery -->
							<?php 	
							$i = 1;
							$dir = count(glob("img/galeria/espacos/sao-paulo/*.jpg"));
								// echo $dir;
							while ($i <= $dir):
								?>
								<div class="col-6 col-md-4 p-0">
									<a href="<?=$caminhoEspacos . 'sao-paulo/' . $i . '.jpg'; ?>">
										<span class="thumb-info thumb-info-no-borders thumb-info-centered-icons">
											<span class="thumb-info-wrapper">
												<img src="<?=$caminhoEspacos . 'sao-paulo/thumbs/' . $i . '.jpg';?>" class="img-fluid" loading="lazy"/>
												<span class="thumb-info-action">
													<span class="thumb-info-action-icon thumb-info-action-icon-light"><i class="fas fa-plus text-dark"></i></span>
												</span>
											</span>
										</span>
									</a>
								</div>
								<?php
								$i++; 
							endwhile;
							?>
						</div>
					</div>
				</div>
			</article>
		</div>
	</div>
</div>
</div>
</section>
<section id="espacos" class="section-dark border-0 pb-3">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 text-center pt-5 pb-lg-4">
				<h3 class="font-weight-bold text-primary text-12">Nossos Espaços</h3>
				<p>O Buffet Metropole possui 3 espaços distintos para agradar aos mais variados estilos.</p>
			</div>
			<div class="col-12 col-lg-4">
				<h4 class="font-weight-normal text-10 text-brown font-GreatVibes">
					Espaço New York
				</h4>
				<ul>
					<li>Capacidade para até 350 convidados</li>
					<li>Ar condicionado central</li>
					<li>Cozinha exclusiva</li>
					<li>Controle de som e iluminação por computador</li>
					<li>Serviço de Vallet</li>
					<li>Sala exclusiva para Noiva ou Debutante
					</li>
				</ul>
				<a href="<?=$url ?>espaco-new-york" class="link-espaco">Para mais informações clique aqui</a>
					<!-- </div>	
						<div class="col-12 col-lg-8 pb-lg-3"> -->
							<div class="slider-espaco pt-3">
								<?php  
								$i = 1;
								while($i <= 3):
									$imgEspaco =  $pastaEspaco. 'new-york/'. $i . '.jpg';
									echo '<img data-lazy="'.$imgEspaco.'" alt="Espaço New York" loading="lazy">';
									$i++;
								endwhile;
								?>
							</div>
						</div>
						<div class="col-12 col-lg-4">
							<h4 class="font-weight-normal text-10 text-brown font-GreatVibes">
								Espaço Paris
							</h4>
							<ul>
								<li>Capacidade para até 400 convidados;</li>
								<li>Salão principal retangular com 350 m² de área;</li>
								<li>Charmoso terraço jardim;</li>
								<li>Salão receptivo com 50 m²;</li>
								<li>Elevador com capacidade para até 9 pessoas;</li>
								<li>Ar condicionado central e sala exclusiva para Noiva ou Debutante.
								</li>

							</ul>
							<a href="<?=$url ?>espaco-paris" class="link-espaco">Para mais informações clique aqui</a>
					<!-- </div>	
						<div class="col-12 col-lg-8 pb-lg-3"> -->
							<div class="slider-espaco pt-3">
								<?php  
								$i = 1;
						// while($i <= 1):
								$imgEspaco =  $pastaEspaco. 'paris/'. $i . '.jpg';

								echo '<img data-lazy="'.$imgEspaco.'" alt="Espaço New York" loading="lazy">';

						// 	$i++;
						// endwhile;
								?>

							</div>
						</div>

						<div class="col-12 col-lg-4">
							<h4 class="font-weight-normal text-10 text-brown font-GreatVibes">
								Espaço São Paulo
							</h4>
							<ul>
								<li>Capacidade para até 350 convidados;</li>
								<li>Ar condicionado central;</li>
								<li>Cozinha exclusiva;</li>
								<li>Controle de som e iluminação por computador;</li>
								<li>Serviço de Vallet;</li>
								<li>Sala exclusiva para Noiva ou Debutante.</li>
							</ul>
							<a href="<?=$url ?>espaco-sao-paulo" class="link-espaco">Para mais informações clique aqui</a>
						<!-- </div>	
							<div class="col-12 col-lg-8 pb-lg-3"> -->
								<div class="slider-espaco pt-3">
									<?php  
									$i = 1;
									while($i <= 3):
										$imgEspaco =  $pastaEspaco. 'sao-paulo/'. $i . '.jpg';
										echo '<img data-lazy="'.$imgEspaco.'" alt="Espaço São Paulo" loading="lazy">';
										$i++;
									endwhile;
									?>
								</div>
							</div>
						</div>
					</div>
				</section>

				<section id="localizacao">
					<div class="col-12 p-4">
						<h4 class="font-weight-bold text-primary text-12 font-GreatVibes text-center pt-4">Localização</h4>
					</div>
					<div class="mapouter">
						<div class="gmap_canvas">
							<iframe width="100%" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=Avenida%20%20Ordem%20e%20Progresso%2C%201085%20-%201%C2%BA%20andar%2C%20S%C3%A3o%20Paulo%20-%20SP.&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
						</div>
					</div>
				</section>

				<?php 
				require 'includes/footer.php';
				?>