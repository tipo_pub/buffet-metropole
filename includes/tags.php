<div class="container pt-4">
    <div class="menu">
        <ul class="tags-palavras">

            <?php
            $keys = array_rand($LinksPalavras, 20);

            $i = 0;
            foreach ($keys as $link) {
                $palavra = $LinksPalavras[$link];
                echo "<li><a href=\"$url$link\" title=\"$palavra\">$palavra</a></li>";

                $i++;
            }
            ?>

        </ul>
    </div>

    <h5 class="tags-palavras mt-4"><strong>Tags:</strong> <?=$keywords;?>.</h5>

    <h6 class="tags-palavras">
        O texto acima "<?=$title;?>" é de direito reservado. Sua reprodução, parcial ou total, mesmo citando nossos links, é proibida sem a autorização do autor.
        Plágio é crime e está previsto no artigo 184 do Código Penal. – Lei n° 9.610-98 sobre os Direitos Autorais
    </h6>
</div>